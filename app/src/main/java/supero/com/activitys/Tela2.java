package supero.com.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Carlos Lima on 1/25/2017.
 */
public class Tela2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela2);

        TextView txtViewname = (TextView) findViewById(R.id.textView);

        Intent i = getIntent();
        String nome = i.getStringExtra("name");

        txtViewname.setText("Seu nome é "+nome);
    }

}
